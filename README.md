# Trang Wed Bán Trang Sức Bạc
## Cách Cài Đặt Source Code
### 1. Tải XAMPP
Tải XAMPP phiên bản 7.4.33

https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/7.4.33/xampp-windows-x64-7.4.33-0-VC15-installer.exe/download

### 2. Cách Truy Cập Trang Web
-Bật XAMPP.

-Nhấn Start khởi động Apache và MySQL.

-Thêm file DOAN vào file XAMPP theo đường dẫn: XAMPP\htdocs\DoAn.

-Mở trình duyệt chrome,firefox,cốc cốc,...

-Search link :

+ a. http://localhost/DoAn/ (giao diện chính của web).

+ b. http://localhost/DoAn/admin/login (form đăng nhập của admin).

+ c. http://localhost/phpmyadmin/ (tạo cơ sở dũ liệu tên shoptrangsuc và import bằng file SQL shoptrangsuc trong XAMPP\htdocs\DoAn\database )

-(a)Thao tác đăng ký tài khoản khách hàng và đặt hàng.

-(b)Sau khi nhập tài khoản của admin khikonchui123@gmail.com và mật khẩu 1212123 sẽ đưa ta đến giao diện trang quản trị của admin và trong đó có thể thêm xóa sửa sản phẩm,..,..,..(http://localhost/DoAn/admin/home).

-(c)Tất cả dữ liệu của web đều nằm trong cơ sở dữ liệu shoptrangsuc.